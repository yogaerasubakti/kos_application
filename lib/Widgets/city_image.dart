import 'package:application_kos/models/city.dart';
import 'package:flutter/material.dart';


class CityImage extends StatelessWidget {

  final City city;

  CityImage(this.city);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(18),
      child: Container(
        height: 150,
        width: 120,
        color: Color(0xffF6F7F8),
        child: Column(
          children: [
            Stack(
              children: [
              Image.asset(
                city.imageURL,
                width: 120,
                height: 102,
                fit: BoxFit.cover,
                ),
              Container(
                width: 50,
                height: 30,
                // child: Image.asset(
                //   "assets/icon_star_solid1.png"
                // ),
              )
              ],
            ),
            SizedBox(
              height: 11,
            ),
            Text(
              city.name,
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      ),
    );
  }
}