import 'package:application_kos/pages/detail_page.dart';
import 'package:application_kos/theme.dart';
import 'package:flutter/material.dart';
import 'package:application_kos/models/space.dart';

class SpaceImage extends StatelessWidget {
  final Space space;

  SpaceImage(this.space);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => DetailPage()));
      },
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(18),
            child: Container(
              width: 100,
              height: 100,
              child: Stack(
                children: [
                  Image.asset(space.imageUrl),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                space.name,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(
                height: 2,
              ),
              Text.rich(
                TextSpan(
                  text: '\$${space.price}',
                  style: TextStyle(
                      fontSize: 14,
                      color: blueColor,
                      fontWeight: FontWeight.bold),
                  children: [
                    TextSpan(
                      text: "/ month",
                      style: TextStyle(
                        fontSize: 14,
                        color: blackColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Text("${space.city} ",
                  style: TextStyle(
                    fontSize: 16,
                    color: greyColor,
                  ))
            ],
          )
        ],
      ),
    );
  }
}
