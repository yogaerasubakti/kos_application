import 'package:flutter/material.dart';
import 'package:application_kos/theme.dart';

class FacilityItem extends StatelessWidget {

  final String name;
  final String imageUrl;
  final int total;

  FacilityItem({this.imageUrl, this.name, this.total});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset(
          imageUrl,
          width: 32,
        ),
        SizedBox(
          height: 8,
        ),
        Text.rich(
          TextSpan(
            text: '$total',
            style: TextStyle(
                fontSize: 14, color: blueColor, fontWeight: FontWeight.bold),
            children: [
              TextSpan(
                text: "$name",
                style: TextStyle(
                  fontSize: 14,
                  color: blackColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
