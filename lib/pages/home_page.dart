import 'package:application_kos/Widgets/city_image.dart';
import 'package:application_kos/Widgets/space_image.dart';
import 'package:application_kos/models/tips.dart';
import 'package:flutter/material.dart';
import 'package:application_kos/theme.dart';
import 'package:application_kos/models/city.dart';
import 'package:application_kos/models/space.dart';
import 'package:application_kos/models/tips_image.dart';
//import 'package:application_kos/pages/detail_page.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: ListView(
          children: [
            Padding(
              padding: EdgeInsets.only(
                left: edge,
              ),
              child: Text(
                "Explore Now",
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w600
                ),
              ),
            ),
            SizedBox(
              height: 2,
            ),
            Padding(
              padding: EdgeInsets.only(
                left: edge,
              ),
              child: Text(
                "Saatnya mencari kos-kosan", style: TextStyle(
                  fontSize: 16,
                ),
              )
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: EdgeInsets.only(
                left: edge,
              ),
              child: Text(
                "Kota yang populer", style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              )
            ),
            SizedBox(
              height: 16,
            ),
            Container(
              height: 150,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  CityImage(
                    City(
                      id: 1,
                      name: "Jakarta",
                      imageURL: "assets/city1.png"
                    )
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  CityImage(
                    City(
                      id: 2,
                      name: "Denpasar",
                      imageURL: "assets/city2.png"
                    )
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  CityImage(
                    City(
                      id: 1,
                      name: "Semarang",
                      imageURL: "assets/city3.png"
                    )
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  CityImage(
                    City(
                      id: 1,
                      name: "Surabaya",
                      imageURL: "assets/city4.png"
                    )
                  ),
                  SizedBox(
                    width: 20,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
             Padding(
              padding: EdgeInsets.only(
                left: edge,
              ),
              child: Text(
                "Saatnya mencari kos-kosan", style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold
                ),
              )
            ),
            SizedBox(
              height: 16,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: edge,
              ),
              child: Column(
                children: [
                  SpaceImage(
                    Space(
                      id: 1,
                      name: "Grantt Jaya",
                      imageUrl: "assets/space1.png",
                      price: 30,
                      city: "Jakarta",
                    )
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SpaceImage(
                     Space(
                      id: 2,
                      name: "Cemara Rahayu",
                      imageUrl: "assets/space4.png",
                      price: 28,
                      city: "Malang",
                    )
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  SpaceImage(
                     Space(
                      id: 3,
                      name: "Sinar Tirta",
                      imageUrl: "assets/space2.png",
                      price: 20,
                      city: "Denpasar",
                    )
                  ),
                 SizedBox(
                    height: 30,
                  ),
                   SpaceImage(
                     Space(
                      id: 2,
                      name: "Morgan Brata",
                      imageUrl: "assets/space3.png",
                      price: 27,
                      city: "Surabaya",
                    )
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: EdgeInsets.only(left: edge),
              child: Text(
                "Tips petunjuk",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              )
            ),
            SizedBox(
              height: 16,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: edge,
              ),
              child: Column(
                children: [
                  TipsImage(
                    Tips(
                      id: 1,
                      title: "Cara Booking",
                      imageUrl: "assets/tips1.png",
                      updateAt: "28 Juni"
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TipsImage(
                    Tips(
                      id: 2,
                      title: "Tips",
                      imageUrl: "assets/tips2.png",
                      updateAt: "1 Juni"
                    ),
                  ),
                ],
              ), 
            ),
            SizedBox(
              height: 50,
            ),
            
          ],
        )
      )
    );
  }
}