import 'package:application_kos/Widgets/facility_item.dart';
import 'package:application_kos/theme.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            bottom: false,
            child: Stack(
              children: [
                Image.asset("assets/thumnail1.png",
                    width: MediaQuery.of(context).size.width,
                    height: 350,
                    fit: BoxFit.cover),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 24,
                    vertical: 30,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Image.asset(
                          "assets/btn_back.png",
                          width: 40,
                        ),
                      ),
                      Image.asset(
                        "assets/btn_wishlist.png",
                        width: 40,
                      ),
                    ],
                  ),
                ),
                ListView(
                  children: [
                    SizedBox(
                      height: 328,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(20),
                        ),
                        color: whiteColor,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 30,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: edge,
                            ),
                            child: Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Grantt Jaya",
                                      style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text.rich(
                                      TextSpan(
                                        text: '\$35',
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: blueColor,
                                            fontWeight: FontWeight.bold),
                                        children: [
                                          TextSpan(
                                            text: "/ month",
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: blackColor,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: edge
                            ),
                            child: Text("Main facilities",
                              style: TextStyle(
                                color: blueColor,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: edge,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                FacilityItem(
                                  name: "Kitchen",
                                  imageUrl: "001-bar-counter.png",
                                  total: 1,
                                ),
                                FacilityItem(
                                  name: "Bedroom",
                                  imageUrl: "002-double-bed.png",
                                  total: 2,
                                ),
                                FacilityItem(
                                  name: "Lemari",
                                  imageUrl: "003-cupboard.png",
                                  total: 2,
                                ),
                              ],
                            ),
                          )
                          // NOTE: Main Facilities

                        ],
                      ),
                    ),
                  ],
                )
              ],
            )));
  }
}
