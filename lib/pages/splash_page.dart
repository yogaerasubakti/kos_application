import 'package:application_kos/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:application_kos/theme.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      bottom: false,
      child: Stack(children: [
        Align(
            alignment: Alignment.bottomCenter,
            child: Image.asset("assets/splash_image.png")),
        Padding(
          padding: EdgeInsets.only(
            top: 50,
            left: 30,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage("assets/logo.png"),
                )),
              ),
              SizedBox(
                height: 30,
              ),
              Text("Open world for\nsearching your rent home",
                  style: TextStyle(
                    color: blackColor,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  )),
              SizedBox(
                height: 10,
              ),
              Text("Mencari sebuah rumah yang\nAman, Tertib, dan Sehat",
                  style: TextStyle(fontSize: 17)),
              SizedBox(
                height: 33,
              ),
              Container(
                width: 210,
                height: 50,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomePage()
                      )
                    );
                  },
                  color: blueColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(17),
                  ),
                  child: Text(
                    "Explore Now",
                    style: TextStyle(
                      color: whiteColor,
                      fontSize: 18,
                    ),
                  ),
                ),
              )
            ],
          ),
        )
      ]),
    ));
  }
}
