import 'package:application_kos/theme.dart';
import 'package:flutter/material.dart';
import 'package:application_kos/models/tips.dart';


class TipsImage extends StatelessWidget {

  final Tips tips;

  TipsImage(this.tips);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image.asset(
          tips.imageUrl,
          width: 100,
        ),
        SizedBox(
          width: 20,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              tips.title,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              tips.updateAt,
              style: TextStyle(
              color: greyColor,   
              ),
            ),
          ],
        ),
        Spacer(
          
        ),
        IconButton(
          onPressed:(){},
          icon: Icon(Icons.chevron_right,
            color: greyColor,
          )
        )
      ],
    );
  }
}
