class Space{
  int id;
  String name;
  String imageUrl;
  int price;
  String city;
  int rating;

  Space({
    this.city,
    this.id,
    this.imageUrl,
    this.name,
    this.price,
    this.rating,
  });
}